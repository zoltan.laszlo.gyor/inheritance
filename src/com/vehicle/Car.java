package com.vehicle;

public class Car extends Vehicle{
    String fuel;
    String color;

    public Car(){
        super(); //meghívod a Vehicle konstruktorát; mindig kell
        this.fuel = "benzin";
        this.color = "blue";
    }

    @Override
    public void move(int x, int y){
        super.move(x * super.getSpeed() , y * super.getSpeed());
    }
}
