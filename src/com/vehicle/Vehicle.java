package com.vehicle;

public class Vehicle {
    private int speed;
    private int weight;
    private int x;
    private int y;

    public Vehicle(){

    }

    public Vehicle(int speed, int weight, int x, int y){
        this.speed = speed;
        this.weight = weight;
        this.x = x;
        this.y = y;
    }

    public void move(int x, int y){
        this.x += x;
        this.y += y;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
